import express from 'express';
import cors from 'cors';

const app = express();
app.use(cors());

app.get('/summ', (req, res) => {
  const summ = (+req.query.a || 0) + (+req.query.b || 0);
  return res.send(summ.toString());
});

app.listen(3000, () => {
  console.log('Example app listening on port 3000!');
});
